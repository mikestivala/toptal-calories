<?php

namespace App;

use App\Events\AddApiTokenToUser;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'daily_calorie_limit', 'is_user_manager', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'creating' => AddApiTokenToUser::class,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'daily_calorie_limit' => 'integer',
        'is_user_manager' => 'boolean',
        'is_admin' => 'boolean',
    ];

    public function meals()
    {
        return $this->hasMany(Meal::class);
    }
}
