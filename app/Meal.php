<?php

namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use App\Events\BustCaloriesConsumedThisDayCache;

class Meal extends Model
{
    protected $fillable = [
        'text',
        'calories',
        'meal_time',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'meal_time'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'calories_consumed_this_day'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'calories_consumed_this_day' => 'integer',
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'creating' => BustCaloriesConsumedThisDayCache::class,
        'updating' => BustCaloriesConsumedThisDayCache::class,
        'deleted' => BustCaloriesConsumedThisDayCache::class,
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeOfUser($query, User $user)
    {
        return $query->where('user_id', $user->id);
    }

    public function getCaloriesConsumedThisDayCacheKey()
    {
        return sprintf(
            "calories-consumed-this-day,user:%d,%s",
            $this->user->id,
            $this->meal_time->format('Y-m-d')
        );
    }

    public function getCaloriesConsumedThisDayAttribute()
    {
        return (int) Cache::rememberForever($this->getCaloriesConsumedThisDayCacheKey(), function () {
            return self::query()
                ->whereUserId($this->user_id)
                ->whereYear('meal_time', $this->meal_time->format('Y'))
                ->whereMonth('meal_time', $this->meal_time->format('m'))
                ->whereDay('meal_time', $this->meal_time->format('d'))
                ->sum('calories');
        });
    }
}
