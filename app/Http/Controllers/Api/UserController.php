<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if (! $request->user()->can('indexUsers', User::class)) {
            abort(403, "You're not authorized to do that.");
        }

        return User::orderBy('created_at', 'desc')->paginate();
    }

    public function store(Request $request)
    {
        if (! $request->user()->can('create', User::class)) {
            abort(403, "You're not authorized to do that.");
        }

        $data = $request->validate([
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6',
            'daily_calorie_limit' => 'required|integer',
            'is_user_manager' => 'boolean',
        ]);

        $data['password'] = Hash::make($data['password']);

        return User::create($data);
    }

    public function update(Request $request, User $user)
    {
        if (! $request->user()->can('update', $user)) {
            abort(403, "You're not authorized to do that.");
        }

        $data = $request->validate([
            'email' => 'email',
            'password' => 'string|min:6',
            'daily_calorie_limit' => 'integer|min:1',
            'is_user_manager' => 'boolean',
            'is_admin' => 'boolean',
        ]);

        if (isset($data['is_admin']) && ! $request->user()->can('makeAdmin', User::class)) {
            abort(403, "You're not authorized to do that.");
        }

        if (isset($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        }

        $user->update($data);

        return $user;
    }

    public function destroy(Request $request, User $user)
    {
        if (! $request->user()->can('delete', $user)) {
            abort(403, "You're not authorized to do that.");
        }

        $user->delete();
    }

    public function show(Request $request, User $user)
    {
        if (! $request->user()->can('view', $user)) {
            abort(403, "You're not authorized to do that.");
        }

        return $user;
    }
}
