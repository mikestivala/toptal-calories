<?php

namespace App\Http\Controllers\Api;

use App\Meal;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MealController extends Controller
{
    public function index(Request $request, User $user)
    {
        if (! $request->user()->can('viewMeals', $user)) {
            abort(403, "You're not authorized to do that.");
        }

        $request->validate([
            'date_from' => 'date',
            'date_to' => 'date',
            'time_from' => 'date_format:H:i',
            'time_to' => 'date_format:H:i',
        ]);

        $query = Meal::ofUser($user)->orderBy('meal_time', 'desc');

        // Date filters
        $query->when($request->has('date_from'), function ($query) use ($request) {
            $query->whereDate('meal_time', '>=', $request->date_from);
        });

        $query->when($request->has('date_to'), function ($query) use ($request) {
            $query->whereDate('meal_time', '<=', $request->date_to);
        });

        // Time filters
        $query->when($request->has('time_from'), function ($query) use ($request) {
            $query->whereRaw("TIME(meal_time) >= '{$request->time_from}'");
        });

        $query->when($request->has('time_to'), function ($query) use ($request) {
            $query->whereRaw("TIME(meal_time) <= '{$request->time_to}'");
        });

        return $query->paginate();
    }

    public function store(Request $request, User $user)
    {
        if (! $request->user()->can('createMealsFor', $user)) {
            abort(403, "You're not authorized to do that.");
        }

        $data = $request->validate([
            'text' => 'required|min:1',
            'calories' => 'required|int|min:0',
            'meal_time' => 'required|date',
        ]);

        return $user->meals()->create($data);
    }

    public function update(Request $request, User $user, Meal $meal)
    {
        if (! $meal->user->is($user)) {
            abort(404);
        }

        if (! $request->user()->can('update', $meal)) {
            abort(403, "You're not authorized to do that.");
        }

        $data = $request->validate([
            'text' => 'min:1',
            'calories' => 'int|min:0',
            'meal_time' => 'date',
        ]);

        $meal->update($data);

        return $meal->fresh();
    }

    public function destroy(Request $request, User $user, Meal $meal)
    {
        if (! $meal->user->is($user)) {
            abort(404);
        }

        if (! $request->user()->can('delete', $meal)) {
            abort(403, "You're not authorized to do that.");
        }

        $meal->delete();
    }
}
