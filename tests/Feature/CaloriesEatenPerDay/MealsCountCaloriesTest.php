<?php

namespace Tests\Feature\Users;

use App\Meal;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MealsCountCaloriesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_calculates_the_calories_eaten_that_day_for_one_meal()
    {
        $meal = factory(Meal::class)->create();
        $this->assertEquals($meal->calories, $meal->calories_consumed_this_day);
    }

    /** @test */
    function it_calculates_the_calories_eaten_that_day_for_multiple_meals()
    {
        $meal = factory(Meal::class)->create([
            'meal_time' => today(),
            'calories' => 50,
        ]);

        factory(Meal::class)->create([
            'meal_time' => today(),
            'calories' => 20,
            'user_id' => $meal->user_id
        ]);

        factory(Meal::class)->create([
            'meal_time' => today(),
            'calories' => 30,
            'user_id' => $meal->user_id
        ]);

        $this->assertEquals(100, $meal->calories_consumed_this_day);
    }

    /** @test */
    function it_calculates_the_calories_eaten_that_day_for_multiple_meals_ignoring_other_days()
    {
        $meal = factory(Meal::class)->create([
            'meal_time' => today(),
            'calories' => 50,
        ]);

        factory(Meal::class)->create([
            'meal_time' => today(),
            'calories' => 20,
            'user_id' => $meal->user_id
        ]);

        factory(Meal::class)->create([
            'meal_time' => today(),
            'calories' => 30,
            'user_id' => $meal->user_id
        ]);

        factory(Meal::class)->create([
            'meal_time' => today()->subDay(),
            'calories' => 300,
            'user_id' => $meal->user_id
        ]);

        $this->assertEquals(100, $meal->calories_consumed_this_day);
    }

    /** @test */
    function it_doesnt_consider_meals_of_other_users()
    {
        $meal = factory(Meal::class)->create([
            'meal_time' => today(),
            'calories' => 50,
        ]);

        $anotherMeal = factory(Meal::class)->create([
            'meal_time' => today(),
            'calories' => 20,
        ]);

        $aThirdMeal = factory(Meal::class)->create([
            'meal_time' => today(),
            'calories' => 30,
        ]);

        $this->assertEquals(50, $meal->calories_consumed_this_day);
        $this->assertEquals(20, $anotherMeal->calories_consumed_this_day);
        $this->assertEquals(30, $aThirdMeal->calories_consumed_this_day);
    }
}
