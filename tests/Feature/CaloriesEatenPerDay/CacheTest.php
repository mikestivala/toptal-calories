<?php

namespace Tests\Feature\CaloriesEatenPerDay;

use App\Meal;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Support\Facades\Cache;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CacheTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_creates_cached_key()
    {
        $meal = factory(Meal::class)->create([
            'meal_time' => Carbon::create('2018', '01', '01'),
        ]);

        $this->assertEquals('calories-consumed-this-day,user:1,2018-01-01', $meal->getCaloriesConsumedThisDayCacheKey());
    }

    /** @test */
    function it_returns_cached_result()
    {
        $meal = factory(Meal::class)->create([
            'meal_time' => Carbon::create('2018', '01', '01'),
        ]);

        $get = Cache::shouldReceive('rememberForever')
            ->once()
            // ->with('2018-01-01')
            ->andReturn(500)
            ->getMock();

        $meal->calories_consumed_this_day;
    }

    /** @test */
    function it_busts_cache_when_meal_is_updated()
    {
        $meal = factory(Meal::class)->create([
            'meal_time' => Carbon::create('2018', '01', '01')
        ]);

        $forget = Cache::shouldReceive('forget')
            ->once()
            ->with('calories-consumed-this-day,user:1,2018-01-01')
            ->getMock();

        $meal->calories = 50;

        $meal->save();
    }

    /** @test */
    function it_updates_cache_when_new_meals_are_added_on_the_same_day()
    {
        $meal = factory(Meal::class)->create([
            'meal_time' => Carbon::create('2018', '01', '01'),
            'calories' => 50,
        ]);

        $this->assertEquals(50, $meal->calories_consumed_this_day);

        factory(Meal::class)->create([
            'user_id' => $meal->user->id,
            'meal_time' => Carbon::create('2018', '01', '01'),
            'calories' => 20,
        ]);

        $this->assertEquals(70, $meal->calories_consumed_this_day);
    }

    /** @test */
    function it_updates_cache_when_meals_on_the_same_day_are_deleted()
    {
        $meal = factory(Meal::class)->create([
            'meal_time' => Carbon::create('2018', '01', '01'),
            'calories' => 50,
        ]);

        $anotherMeal = factory(Meal::class)->create([
            'user_id' => $meal->user->id,
            'meal_time' => Carbon::create('2018', '01', '01'),
            'calories' => 20,
        ]);

        $this->assertEquals(70, $meal->calories_consumed_this_day);

        $anotherMeal->delete();

        $this->assertEquals(50, $meal->calories_consumed_this_day);
    }
}
