<?php

namespace Tests\Feature\Api\Users;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ShowUserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_shows_user()
    {
        $user = factory(User::class)->create();
        $manager = factory(User::class)->states('user-manager')->create();

        $this->actingAs($manager, 'api')
            ->json("GET", "/api/users/{$user->id}")
            ->assertStatus(200)
            ->assertSee($user->email);
    }

    /** @test */
    function normal_users_cant_retrieve_other_users()
    {
        $user = factory(User::class)->create();
        $anotherUser = factory(User::class)->create();

        $this->actingAs($anotherUser, 'api')
            ->json("GET", "/api/users/{$user->id}")
            ->assertStatus(403);
    }
}
