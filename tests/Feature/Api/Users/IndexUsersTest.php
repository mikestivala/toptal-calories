<?php

namespace Tests\Feature\Api\Users;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IndexUsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_indexes_users()
    {
        $users = factory(User::class, 10)->create();

        $this->actingAs(factory(User::class)->states('user-manager')->create(), 'api')
            ->json('GET', '/api/users')
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    [
                        'id' => $users[0]->id,
                        'email' => $users[0]->email,
                    ]
                ]
            ]);
    }

    /** @test */
    function it_forbids_regular_users_from_indexing_users()
    {
        $this->actingAs(factory(User::class)->create(), 'api')
            ->json('GET', '/api/users')
            ->assertStatus(403);
    }
}
