<?php

namespace Tests\Feature\Api\Users;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateUserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_user_manager_can_update_a_user()
    {
        $manager = factory(User::class)->states('user-manager')->create();
        $user = factory(User::class)->create();

        $this->actingAs($manager, 'api')
            ->json("PUT", "/api/users/{$user->id}", [
                'email' => 'john@john.com',
                'password' => 'secret',
            ])
            ->assertStatus(200);

        $user->refresh();
        $this->assertEquals('john@john.com', $user->email);
        $this->assertTrue(Hash::check('secret', $user->password));
    }

    /** @test */
    function a_user_manager_can_update_specific_fields_of_a_user()
    {
        $manager = factory(User::class)->states('user-manager')->create();
        $user = factory(User::class)->create();

        $this->actingAs($manager, 'api')
            ->json("PUT", "/api/users/{$user->id}", [
                'email' => 'john@john.com',
            ])
            ->assertStatus(200);

        $user->refresh();
        $this->assertEquals('john@john.com', $user->email);
    }

    /** @test */
    function a_user_can_update_himself()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user, 'api')
            ->json("PUT", "/api/users/{$user->id}", [
                'email' => 'john@john.com',
            ])
            ->assertStatus(200);

        $user->refresh();
        $this->assertEquals('john@john.com', $user->email);
    }

    /** @test */
    function a_user_cant_update_someone_else()
    {
        $user = factory(User::class)->create();

        $this->actingAs(factory(User::class)->create(), 'api')
            ->json("PUT", "/api/users/{$user->id}", [
                'email' => 'john@john.com',
            ])
            ->assertStatus(403);
    }

    /** @test */
    function an_admin_can_make_another_user_an_admin()
    {
        $user = factory(User::class)->create();
        $admin = factory(User::class)->states('admin')->create();

        $this->actingAs($admin, 'api')
            ->json("PUT", "/api/users/{$user->id}", [
                'is_admin' => true,
            ])
            ->assertStatus(200);

        $this->assertTrue($user->refresh()->is_admin);
    }

    /** @test */
    function a_user_manager_cant_make_another_user_an_admin()
    {
        $user = factory(User::class)->create();
        $manager = factory(User::class)->states('user-manager')->create();

        $this->actingAs($manager, 'api')
            ->json("PUT", "/api/users/{$user->id}", [
                'is_admin' => true,
            ])
            ->assertStatus(403);

        $this->assertFalse($user->refresh()->is_admin);
    }
}
