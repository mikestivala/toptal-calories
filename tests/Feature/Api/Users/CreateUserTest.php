<?php

namespace Tests\Feature\Api\Users;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateUserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_creates_a_user()
    {
        $user = factory(User::class)->states('user-manager')->create();

        $this->actingAs($user, 'api')
            ->json("POST", "/api/users", [
                'name' => 'John',
                'email' => 'john@john.com',
                'daily_calorie_limit' => 500,
                'password' => 'secret',
            ])
            ->assertStatus(201);
    }

    /** @test */
    function only_user_managers_can_create_users()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user, 'api')
            ->json("POST", "/api/users", [
                'name' => 'John',
                'email' => 'john@john.com',
                'password' => 'secret',
            ])
            ->assertStatus(403);
    }
}
