<?php

namespace Tests\Feature\Api\Users;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteUserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function a_user_manager_can_delete_a_user()
    {
        $manager = factory(User::class)->states('user-manager')->create();
        $user = factory(User::class)->create();

        $this->actingAs($manager, 'api')
            ->json("DELETE", "/api/users/{$user->id}")
            ->assertStatus(200);

        $this->assertDatabaseMissing('users', ['email' => 'john@john.com']);
    }

    /** @test */
    function users_cant_delete_other_users()
    {
        $user = factory(User::class)->create();

        $this->actingAs(factory(User::class)->create(), 'api')
            ->json("DELETE", "/api/users/{$user->id}")
            ->assertStatus(403);

        $this->assertDatabaseMissing('users', ['email' => 'john@john.com']);
    }
}
