<?php

namespace Tests\Feature\Api\Meals;

use App\Meal;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IndexMealsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_indexes_meals_by_user()
    {
        $user = factory(User::class)->create();
        $meals = factory(Meal::class, 5)->create([
            'user_id' => $user->id,
        ]);

        $meal = Meal::orderBy('meal_time', 'desc')->first();

        $this->actingAs($user, 'api')
            ->json('GET', "/api/users/{$user->id}/meals")
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    [
                        'id' => $meal->id,
                        'text' => $meal->text,
                    ]
                ]
            ]);
    }

    /** @test */
    function it_doesnt_show_other_users_meals()
    {
        $user = factory(User::class)->create();
        $meals = factory(Meal::class, 2)->create([
            'user_id' => $user->id,
        ]);

        $meal = factory(Meal::class)->create([
            'text' => 'Not my meal!'
        ]);

        $this->actingAs($user, 'api')
            ->json('GET', "/api/users/{$user->id}/meals")
            ->assertStatus(200)
            ->assertDontSee($meal->text);
    }

    /** @test */
    function it_doesnt_allow_users_to_see_other_users_meals()
    {
        $users = factory(User::class, 2)->create();

        $this->actingAs($users[0], 'api')
            ->json('GET', "/api/users/{$users[1]->id}/meals")
            ->assertStatus(403);
    }
}
