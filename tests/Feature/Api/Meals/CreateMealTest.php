<?php

namespace Tests\Feature\Api\Meals;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateMealTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_allows_users_to_create_meals()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user, 'api')
            ->post("/api/users/{$user->id}/meals", [
                'text' => 'Vegetable Bowl',
                'meal_time' => '2018-06-06 12:05:00',
                'calories' => 500
            ])
            ->assertStatus(201);

        $this->assertDatabaseHas('meals', [
            'text' => 'Vegetable Bowl',
            'meal_time' => '2018-06-06 12:05:00',
            'calories' => 500
        ]);
    }

    /** @test */
    function it_stops_users_from_creating_meals_for_others()
    {
        $users = factory(User::class, 2)->create();

        $this->actingAs($users[0], 'api')
            ->json("POST", "/api/users/{$users[1]->id}/meals", [
                'text' => 'Vegetable Bowl',
                'meal_time' => '2018-06-06 12:05:00',
                'calories' => 500
            ])
            ->assertStatus(403);
    }

    /** @test */
    function it_allows_admins_to_create_meals_for_others()
    {
        $user = factory(User::class)->create();
        $admin = factory(User::class)->states('admin')->create();

        $this->actingAs($admin, 'api')
            ->json("POST", "/api/users/{$user->id}/meals", [
                'text' => 'Vegetable Bowl',
                'meal_time' => '2018-06-06 12:05:00',
                'calories' => 500
            ])
            ->assertStatus(201);
    }

    /** @test */
    function it_validates_required_data()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user, 'api')
            ->json("POST", "/api/users/{$user->id}/meals")
            ->assertStatus(422)
            ->assertJsonStructure([
                'errors' => [
                    'text', 'meal_time', 'calories'
                ]
            ]);
    }
}
