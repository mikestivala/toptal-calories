<?php

namespace Tests\Feature\Api\Meals;

use App\Meal;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteMealTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_deletes_a_users_meal()
    {
        $meal = factory(Meal::class)->create();

        $this->actingAs($meal->user, 'api')
            ->json("DELETE", "/api/users/{$meal->user->id}/meals/{$meal->id}")
            ->assertStatus(200);

        $this->assertDatabaseMissing('meals', [
            'text' => $meal->text,
        ]);
    }

    /** @test */
    function users_cant_delete_someone_elses_meal()
    {
        $meal = factory(Meal::class)->create();

        $this->actingAs(factory(User::class)->create(), 'api')
            ->json("DELETE", "/api/users/{$meal->user->id}/meals/{$meal->id}")
            ->assertStatus(403);
    }

    /** @test */
    function it_cant_find_a_meal_with_the_wrong_users()
    {
        $meal = factory(Meal::class)->create();
        $user = factory(User::class)->create();

        $this->actingAs($meal->user, 'api')
            ->json("DELETE", "/api/users/{$user->id}/meals/{$meal->id}")
            ->assertStatus(404);
    }
}
