<?php

namespace Tests\Feature\Api\Meals;

use App\Meal;
use App\User;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IndexMealsFiltersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_filters_by_date()
    {
        $user = factory(User::class)->create();

        factory(Meal::class)->create([
            'user_id' => $user->id,
            'meal_time' => Carbon::create('2018', '01', '01'),
            'text' => 'January Meal'
        ]);

        factory(Meal::class)->create([
            'user_id' => $user->id,
            'meal_time' => Carbon::create('2018', '02', '01'),
            'text' => 'February Meal'
        ]);

        factory(Meal::class)->create([
            'user_id' => $user->id,
            'meal_time' => Carbon::create('2018', '03', '01'),
            'text' => 'March Meal'
        ]);

        $this->actingAs($user, 'api')
            ->json('GET', "/api/users/{$user->id}/meals", [
                'date_from' => '2018-01-01',
                'date_to' => '2018-02-01',
            ])
            ->assertStatus(200)
            ->assertSee('January Meal')
            ->assertSee('February Meal')
            ->assertDontSee('March Meal');

        $this->actingAs($user, 'api')
            ->json('GET', "/api/users/{$user->id}/meals", [
                'date_from' => '2018-02-01',
                'date_to' => '2018-03-01',
            ])
            ->assertStatus(200)
            ->assertDontSee('January Meal')
            ->assertSee('February Meal')
            ->assertSee('March Meal');
    }

    /** @test */
    function it_filters_by_time()
    {
        $user = factory(User::class)->create();

        factory(Meal::class)->create([
            'user_id' => $user->id,
            'meal_time' => Carbon::create('2018', '01', '01', '12', '30'),
            'text' => 'January Lunch Meal'
        ]);

        factory(Meal::class)->create([
            'user_id' => $user->id,
            'meal_time' => Carbon::create('2018', '02', '01', '13', '15'),
            'text' => 'February Lunch Meal'
        ]);

        factory(Meal::class)->create([
            'user_id' => $user->id,
            'meal_time' => Carbon::create('2018', '03', '01', '20', '05'),
            'text' => 'March Dinner Meal'
        ]);

        $this->actingAs($user, 'api')
            ->json('GET', "/api/users/{$user->id}/meals", [
                'time_from' => '12:00',
                'time_to' => '14:00',
            ])
            ->assertStatus(200)
            ->assertSee('January Lunch Meal')
            ->assertSee('February Lunch Meal')
            ->assertDontSee('March Dinner Meal');

        $this->actingAs($user, 'api')
            ->json('GET', "/api/users/{$user->id}/meals", [
                'time_from' => '13:00',
                'time_to' => '20:30',
            ])
            ->assertStatus(200)
            ->assertDontSee('January Lunch Meal')
            ->assertSee('February Lunch Meal')
            ->assertSee('March Dinner Meal');
    }

    /** @test */
    function it_validates()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user, 'api')
            ->json('GET', "/api/users/{$user->id}/meals", [
                'date_from' => 'invalid',
                'date_to' => 'invalid',
                'time_from' => 'invalid',
                'time_to' => 'invalid',
            ])->assertStatus(422)
                ->assertJsonStructure([
                    'errors' => [
                        'date_from',
                        'date_to',
                        'time_from',
                        'time_to',
                    ]
                ]);
    }
}
