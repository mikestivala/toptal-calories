<?php

namespace Tests\Feature\Api\Meals;

use App\Meal;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateMealTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_updates_a_users_meal()
    {
        $meal = factory(Meal::class)->create();

        $this->actingAs($meal->user, 'api')
            ->json("PUT", "/api/users/{$meal->user->id}/meals/{$meal->id}", [
                'text' => 'Vegetable Bowl',
                'meal_time' => '2018-06-06 12:05:00',
                'calories' => 500
            ])->assertStatus(200);

        $meal->refresh();
        $this->assertEquals($meal->text, 'Vegetable Bowl');
        $this->assertEquals($meal->calories, 500);
        $this->assertEquals((string) $meal->meal_time, '2018-06-06 12:05:00');
    }

    /** @test */
    function users_cant_update_someone_elses_meal()
    {
        $meal = factory(Meal::class)->create();

        $this->actingAs(factory(User::class)->create(), 'api')
            ->json("PUT", "/api/users/{$meal->user->id}/meals/{$meal->id}", [
                'text' => '',
                'meal_time' => '2018-0d6-06 12:05:00',
                'calories' => 'five'
            ])->assertStatus(403);
    }

    /** @test */
    function it_cant_update_a_meal_with_invalid_data()
    {
        $meal = factory(Meal::class)->create();

        $this->actingAs($meal->user, 'api')
            ->json("PUT", "/api/users/{$meal->user->id}/meals/{$meal->id}", [
                'text' => '',
                'meal_time' => '2018-0d6-06 12:05:00',
                'calories' => 'five'
            ])->assertStatus(422);
    }

    /** @test */
    function it_cant_find_a_meal_with_the_wrong_users()
    {
        $meal = factory(Meal::class)->create();
        $user = factory(User::class)->create();

        $this->actingAs($meal->user, 'api')
            ->json("PUT", "/api/users/{$user->id}/meals/{$meal->id}")
            ->assertStatus(404);
    }
}
