<?php

namespace Tests\Browser;

use App\Meal;
use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class MealTest extends DuskTestCase
{
    use DatabaseMigrations;

    /** @test */
    function it_indexes_meals()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            $meal = factory(Meal::class)->create([
                'user_id' => $user->id,
            ]);

            $browser->loginAs($user)
                    ->visit('/app')
                    ->assertSee($meal->text);
        });
    }

    /** @test */
    function it_creates_meals()
    {
        $this->browse(function (Browser $browser) {
            $user = factory(User::class)->create();

            $browser->loginAs($user)
                    ->visit('/app')
                    ->type('@text', 'Veggie Salad')
                    ->type('calories', '200')
                    ->click('@save-meal-button')
                    ->waitForText('1 of 1')
                    ->assertSee('Veggie Salad');

            $browser->loginAs($user)
                    ->visit('/app')
                    ->type('@text', 'Tuna Salad')
                    ->type('calories', '200')
                    ->click('@save-meal-button')
                    ->waitForText('2 of 2')
                    ->assertSee('Veggie Salad')
                    ->assertSee('Tuna Salad');

            $this->assertDatabaseHas('meals', ['text' => 'Veggie Salad']);
            $this->assertDatabaseHas('meals', ['text' => 'Tuna Salad']);
        });
    }

    /** @test */
    function it_deletes_meal()
    {
        $this->browse(function (Browser $browser) {
            $meal = factory(Meal::class)->create([
                'text' => 'Pizza'
            ]);

            $browser->loginAs($meal->user)
                    ->visit('/app')
                    ->waitForText('Pizza')
                    ->clickLink('Delete')
                    ->waitForText('Continue')
                    ->press('Continue')
                    ->waitForText('deleted');

            $this->assertDatabaseMissing('meals', ['text' => 'Pizza']);
        });
    }

    /** @test */
    function it_updates_meal()
    {
        $this->browse(function (Browser $browser) {
            $meal = factory(Meal::class)->create([
                'text' => 'Pizza'
            ]);

            $browser->loginAs($meal->user)
                    ->visit('/app')
                    ->waitForText('Pizza')
                    ->clickLink('Edit')
                    ->type('@meal-text', 'Calzone')
                    ->press('@update-meal')
                    ->waitForText('updated')
                    ->assertSee('Calzone')
                    ->assertDontSee('Pizza');

            $this->assertDatabaseMissing('meals', ['text' => 'Pizza']);
            $this->assertDatabaseHas('meals', ['text' => 'Calzone']);
        });
    }
}
