<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserManagerTest extends DuskTestCase
{
    use DatabaseMigrations;

    /** @test */
    function it_indexes_user()
    {
        $this->browse(function (Browser $browser) {
            $manager = factory(User::class)->states('user-manager')->create();

            $users = factory(User::class, 5)->create();

            $browser->loginAs($manager)
                    ->visit('/users')
                    ->assertSee($users[0]->email);
        });
    }
}
