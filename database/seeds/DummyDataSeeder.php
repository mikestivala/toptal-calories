<?php

use App\Meal;
use App\User;
use Illuminate\Database\Seeder;

class DummyDataSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 10)->create()->each(function ($user) {
            factory(Meal::class, 50)->create([
                'user_id' => $user->id,
            ]);
        });
    }
}
