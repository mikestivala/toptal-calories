<?php

use App\Meal;
use App\User;
use Faker\Generator as Faker;

$factory->define(Meal::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'text' => $faker->sentence,
        'meal_time' => $faker->dateTime,
        'calories' => $faker->randomNumber(),
    ];
});
