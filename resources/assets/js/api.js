let api = {
    getUser(userId) {
        return axios.get('/api/users/' + userId)
    },

    login(data) {
        return axios.post('/login', data)
    },

    requestPasswordReset(email) {
        return axios.post('/password/email', {
            email: email
        })
    },

    resetPassword(data) {
        return axios.post('/password/reset', data)
    },

    logout() {
        return axios.post('/logout')
    },

    register(data) {
        return axios.post('/register', data)
    },

    getUserMeals(userId, filters)
    {
        return axios.get('/api/users/' + userId + '/meals', {params: filters})
    },

    createUserMeal(userId, meal)
    {
        return axios.post('/api/users/' + userId + '/meals', meal)
    },

    updateUserMeal(userId, mealId, meal)
    {
        return axios.patch('/api/users/' + userId + '/meals/' + mealId, meal)
    },

    deleteUserMeal(userId, mealId)
    {
        return axios.delete('/api/users/' + userId + '/meals/' + mealId)
    },

    updateUser(userId, user)
    {
        return axios.patch('/api/users/' + userId, user)
    },

    getUsers()
    {
        return axios.get('/api/users/')
    },

    deleteUser(userId)
    {
        return axios.delete('/api/users/' + userId)
    },

    createUser(user)
    {
        return axios.post('/api/users/', user)
    },
}

module.exports = api;
