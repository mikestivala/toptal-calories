
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Toasted from 'vue-toasted';
import VueRouter from 'vue-router';
import VuejsDialog from "vuejs-dialog"
import VueTimepicker from 'vue2-timepicker'

Vue.use(Toasted);
Vue.use(VueRouter);
Vue.use(VuejsDialog);
Vue.use(VueTimepicker);

Vue.component('calorie-counter', require('./components/Wrapper.vue'));
Vue.component('meal-index', require('./components/MealIndex.vue'));
Vue.component('meal-index-filters', require('./components/MealIndexFilters.vue'));
Vue.component('meal-create-form', require('./components/CreateMealForm.vue'));
Vue.component('meal', require('./components/Meal.vue'));
Vue.component('user-index', require('./components/UserIndex.vue'));
Vue.component('user', require('./components/User.vue'));
Vue.component('user-create-form', require('./components/CreateUserForm.vue'));

const routes = [
  { path: '/', component: require('./components/pages/Landing.vue') },
  { path: '/app', component: require('./components/pages/App.vue') },
  { path: '/login', component: require('./components/pages/Login.vue') },
  { path: '/password-reset', component: require('./components/pages/ForgotPassword.vue') },
  { path: '/password/reset/:id', component: require('./components/pages/ResetPassword.vue') },
  { path: '/register', component: require('./components/pages/Register.vue') },
  { path: '/profile', component: require('./components/pages/EditProfile.vue') },
  { path: '/users', component: require('./components/pages/Users.vue') },
  { path: '/users/:id/meals', component: require('./components/pages/UserMeals.vue') },
]

const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
})

const app = new Vue({
  router: router,
  data: {store: require('./store')}
}).$mount('#app')
