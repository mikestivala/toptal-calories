let api = require('./api');

let store = {
    state: {
        user: null,
    },

    setUser(user) {
        console.log('store.js - [setUser]')
        this.state.user = user;

        if (user) {
            window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + user.api_token;
        } else {
            window.axios.defaults.headers.common['Authorization'] = '';
        }
    },
}

// If the user can be retrieved from the session...
if (options.user) {
    store.setUser(options.user);
}
module.exports = store;
