<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Authentication Routes...
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', function () {
    return view('app');
})->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// Refresh CSRF token...
Route::get('csrf', function () {
    return ['token' => csrf_token()];
});

// SPA...
Route::get('/{any}', function () {
    return view('app');
})->where('any', '.*');
